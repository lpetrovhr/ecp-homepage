---
url: /work/neos/index.html
pageHeading: NEOs
pageDescription: Lighting Up a Greener Earth
pageHeadingImage: /images/work-shots/neos-screenshot.png
pageHeadingImageAlt: A screenshot from the NEOs project.
pageHeadingImageFormat: desktop
id: neos
published: false
layout: case-study.ejs
sortOrder: 8
---

<p class="paragraph--major"><a href="http://www.neos-ict.com/">NEOs</a> is an energy efficiency technology consultant that was tasked with building a tool to connect energy efficient street lamps all across Croatia. With a strict deadline, NEOs approached East Coast Product to augment their product team and build a clean user interface that lets Croatian cities and towns track LED life span and schedule brightness changes to optimize energy efficiency. Together, we beat the deadline and built a product that has been implemented in over 150,000 street lamps in Croatia.</p>

<h1 class="text-heading-one">Internet of Things</h1>

<p>Each lamp contains a module that has a basic WiFi connector and a small processor. The module connects each lamp into the network of street lamps and with the controller application we built.</p>

<h1 class="text-heading-one">Hardware Integration</h1>

<p>Integrating with unique hardware was a challenge that forced our team to build for a module with low processing power. We were able to do exactly that without sacrificing functionality using the highly performant Node.js.</p>

<h1 class="text-heading-one">Energy Efficiency</h1>

<p>By building brightness control and scheduling functionality into the application, Croatian municipalities can fully optimize energy usage.</p>

<h1 class="text-heading-one">Tools &amp; Techniques</h1>

<ul>
  <li><a href="/technologies/node">Node.js</a></li>
  <li><a href="/technologies/react">React</a></li>
</ul>
