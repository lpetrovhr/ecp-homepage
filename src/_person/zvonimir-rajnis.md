---
name: Zvonimir Rajniš
id: zvonimir-rajnis
numberId: 20
title: Developer
bio: Zvonimir is a JavaScript enthusiast who likes to listen to funk, eat pizza and play football.
areas:
contact: { email: zvonimir, linkedin: https://www.linkedin.com/in/zvonimir-rajni%C5%A1-061462131/, github: https://github.com/zrajnis }
---
