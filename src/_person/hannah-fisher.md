---
name: Hannah Fisher
id: hannah-fisher
numberId: 19
title: Executive Assistant
bio: Hannah makes sure that our Boston office is an amazing place to work.
areas:
contact: { email: hannah }
---
