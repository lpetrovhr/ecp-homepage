---
url: /jobs/lead-javascript-developer/index.html
pageHeading: Lead JavaScript Developer
pageDescription: Zagreb, Croatia
pageHeadingImage:
pageHeadingImageAlt:
id: lead-javascript-developer
hiringContactId: ivan-blazevic
jobType: developer
published: true
layout: job.ejs
---

<p>East Coast Product is looking for an experienced Lead JavaScript Developer to help us build a new team in Zagreb.</p>

<p>This person is an expert in web development and can come up with solutions to complex problems. They are a true expert in their specialty and very skilled in the entire web development process. They are a skilled communicator and can convey and assert technical authority when communicating with client leads. They are able to provide very precise estimations on complex technical challenges.</p>

<p>They are able to architect complex solutions and can support the team to finish projects on time and up to quality standards. They are able to oversee and make sure that multiple teams are on the track to successfully finish their respective projects. This person is able to provide guidance to more junior developers and help them grow professionally. They are able to look at the product from the client’s perspective and make sure that all client needs are satisfied.</p>

<h2 class="text-heading-two">Main Responsibilities</h2>

<ul>
  <li>Skilled and personable communicator, able to convey complex technological concepts to clients with ease and authority</li>
  <li>Can architect solutions to complex problems and make sure that team(s) meet the deadlines and produce quality code</li>
  <li>Makes sure the software meets all requirements of quality, security, modifiability, extensibility etc.</li>
  <li>Making sure the company is following the latest technology trends and introducing them in the team</li>
  <li>Recruiting and growing the Zagreb team based on business needs</li>
</ul>

<h2 class="text-heading-two">Skills and Qualifications</h2>

<ul>
  <li>Strong proficiency in JavaScript</li>
  <li>Knowledge of Node.js and frameworks available for it such as Express or Koa</li>
  <li>Understanding of modern front-end frameworks, experience with React is a plus</li>
  <li>Understanding of asynchronous programming and its quirks and workarounds</li>
  <li>Skilled in application optimization for speed and scalability</li>
  <li>Desire to attain new skills in business, management, and technology</li>
  <li>Demonstrated effective team management skills</li>
  <li>Excellent written and spoken communication in both Croatian and English</li>
  <li>Ability to multi-task and perform well under pressure</li>
</ul>

<h2 class="text-heading-two">Benefits</h2>

<ul>
  <li>Competitive salary commensurate on experience.</li>
  <li>Flexible work schedule.</li>
  <li>Unlimited vacation with a 4 week minimum.</li>
  <li>Casual company culture that values performance and teamwork.</li>
  <li>Opportunities for international travel.</li>
</ul>
