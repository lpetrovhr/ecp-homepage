---
url: /jobs/project-manager/index.html
pageHeading: Project Manager
pageDescription: Split, Croatia
pageHeadingImage:
pageHeadingImageAlt:
id: project-manager
hiringContactId: ivan-blazevic
jobType: other
published: false
layout: job.ejs
---

<p>If you want to take your project management experience to the next level and grow your career alongside a team of passionate JavaScript experts, join our team in Split!</p>

<p>We are looking to bring on motivated and talented individuals who share our passion for learning and development. At East Coast Product, building a diverse team with a collaborative work culture has always been a top priority. Since our founding, we have fostered a challenging and motivating work environment. With our sights set on employee satisfaction, we provide our team members with everything they will need grow their skills and expand their career.</p>

<h2 class="text-heading-two">Responsibilities</h2>

<ul>
  <li>Manage and oversee multiple development projects of varying size and complexity</li>
  <li>Monitor and report on project progress and performance</li>
  <li>Manage detailed project planning and set in place controls to make sure the project is delivered on time</li>
  <li>Prepare reports and documentation, and regularly update senior management and clients</li>
  <li>Identify opportunities for staff development and training and ensure that the training takes place</li>
  <li>Responsible for quickly recognizing, addressing, mitigating, and documenting potential project issues, risks, or delays</li>
  <li>Facilitate clear and regular communication and cooperation among all project stakeholders to ensure the project meets business needs</li>
</ul>

<h2 class="text-heading-two">Skills and Qualifications</h2>

<ul>
  <li>Bachelor’s degree or comparable work experience (2 years minimum)</li>
  <li>Outstanding planning and organizational skills</li>
  <li>Excellent written and verbal Croatian and English communication skills with an intuitive understanding of how best to share knowledge, gain buy-in, and influence decision making</li>
  <li>Demonstrable track record of leadership skills including client and team management</li>
  <li>Ability to resolve conflicts and misunderstandings quickly</li>
  <li>Strong analytical skills with the ability to generate reports</li>
  <li>Ability to multi-task and perform well under pressure</li>
  <li>Desire to attain new skills in business, management, and technology</li>
  <li>Basic understanding of Agile methodologies</li>
  <li>Basic understanding of web development process</li>
</ul>

<h2 class="text-heading-two">Benefits</h2>

<ul>
  <li>Competitive salary commensurate on experience.</li>
  <li>Flexible work schedule.</li>
  <li>Unlimited vacation with a 4 week minimum.</li>
  <li>Casual company culture that values performance and teamwork.</li>
  <li>Opportunities for international travel.</li>
</ul>
