---
url: /jobs/executive-assistant-to-the-cto/index.html
pageHeading: Executive Assistant to the CTO
pageDescription: Split, Croatia
pageHeadingImage:
pageHeadingImageAlt:
id: executive-assistant-to-the-cto
hiringContactId: hannah-fisher
jobType: other
published: false
layout: job.ejs
---

<p>East Coast Product is looking for an experienced Executive Assistant to support our CTO and manage our office in Split, Croatia.  We are looking for a multi-tasking and self-motivated individual with a strong track-record of providing exceptional administrative support. This role requires attention to detail, the ability to meet deadlines, strong organizational skills, and the initiative to grow ECP’s company culture with little to no guidance.</p>

<h2 class="text-heading-two">Main Responsibilities</h2>

<ul>
  <li>Help the CTO scheduling, travel arrangements, task management, and preparation for meetings and presentations.</li>
  <li>Manage our Split office and ensure it is operating efficiently and within budget.</li>
  <li>Create a high level of awareness about ECP and open positions through marketing, community events, and meetups.</li>
  <li>Undertake team building activities like team lunches, office celebrations, etc.</li>
  <li>Assist in local hiring and onboarding.</li>
</ul>

<h2 class="text-heading-two">Skills &amp; Qualifications</h2>

<ul>
  <li>2+ years of demonstrable experience in high-level administrative support.</li>
  <li>Desire to attain new skills in operations, office management, and public relations.</li>
  <li>Excellent written and spoken communication in both Croatian and English.</li>
  <li>Ability to multi-task and perform well under pressure.</li>
  <li>Must be highly detail-oriented, proactive, and possess a strong work ethic.</li>
</ul>

<h2 class="text-heading-two">Benefits</h2>

<ul>
  <li>Competitive salary commensurate with experience.</li>
  <li>Flexible work schedule.</li>
  <li>Unlimited vacation with a 4 week minimum.</li>
  <li>Casual company culture that values performance and teamwork.</li>
  <li>Opportunities for international travel.</li>
</ul>
