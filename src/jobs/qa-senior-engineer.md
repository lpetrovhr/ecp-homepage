---
url: /jobs/qa-senior-engineer/index.html
pageHeading: Senior QA Engineer
pageDescription: Split or Zagreb, Croatia
pageHeadingImage:
pageHeadingImageAlt:
id: qa-senior-engineer
hiringContactId: ivan-blazevic
jobType: developer
published: false
layout: job.ejs
---

<p>Grow your career as a Senior QA Engineer at a growing company software development agency in Split and Zagreb alongside passionate JavaScript experts!</p>

<p>We are looking to bring on motivated and talented individuals who share our passion for learning and development. At East Coast Product, building a diverse team with a collaborative work culture has always been a top priority. Since our founding, we have fostered a challenging and motivating work environment. With our sights set on employee satisfaction, we provide our team members with everything they will need grow their skills and expand their career.</p>

<h2 class="text-heading-two">Responsibilities</h2>

<ul>
  <li>Develop and implement quality assurance measures and testing standards for new applications and enhancements to existing applications  in various stages of the development/product lifecycle.</li>
  <li>Create and execute test strategies that will ensure optimal application performance according to client needs and specifications.</li>
  <li>Ensure that testing activities allow applications to meet business requirements and systems goals, fulfill end-user requirements, and identify existing or potential issues. </li>
  <li>Make process recommendations to improve the development process across the company.</li>
  <li>Analyze formal test results in order to discover and report any defects, bugs, errors, configuration issues, and interoperability flaws.</li>
  <li>Work closely with project and technical leads to promptly address and escalate issues, that affect product delivery and quality</li>
</ul>

<h2 class="text-heading-two">Skills and Qualifications</h2>

<ul>
  <li>3+ years of experience developing automated checks</li>
  <li>Solid grasp of Agile software development and testing methodologies</li>
  <li>Proactive attitude to help drive new QA processes and ideas within the agency</li>
  <li>Good understanding of how to execute Context Driven testing</li>
  <li>Excellent verbal and written communication skills both in Croatian and English</li>
  <li>Strong motivation to learn and teach others</li>
  <li>Great knowledge of JavaScript based testing</li>
</ul>

<h2 class="text-heading-two">Benefits</h2>

<ul>
  <li>Competitive salary commensurate on experience.</li>
  <li>Flexible work schedule.</li>
  <li>Unlimited vacation with a 4 week minimum.</li>
  <li>Casual company culture that values performance and teamwork.</li>
  <li>Opportunities for international travel.</li>
</ul>
